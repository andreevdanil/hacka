from flask import Flask, render_template, jsonify, request

state = {
  'coffee': False,
  'tv':  False,
  'music': False,
  'light': False
}

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index2.html')

@app.route('/update', methods=['GET'])
def postState():
    print(state)
    return jsonify(state)

@app.route('/post', methods=['POST'])
def getState():
    global state
    state = request.json
    print(state)
    return ('', 204)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=9696, debug=True)

