import rec
import slava
import requests
import socket
import pickle
import time
import json

HOST, PORT = "169.254.185.116", 20345
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((HOST, PORT))
sock.listen()
ssock, addr = sock.accept()
print(ssock, addr)

state = {
  'coffee': False,
  'tv':  False,
  'music': False,
  'light': False
}

while True:
    key = input("Enter anykey to record voice: ")
    print(key)
    rec.record()
    device, val = slava.recognize("file.wav")
    state[device] = val

    r = requests.post('http://127.0.0.1:9696/post', json=state)

    jsonObj = pickle.dumps(state)
    try:
        ssock.sendall(jsonObj)
    except Exception as e:
        print(e)
        break

ssock.close()
sock.close()
